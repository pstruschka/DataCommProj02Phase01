package io.muic.dcom.p2;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;


class DataModel {
    public static class ParcelObserved {
        private String parcelId;
        private String stationId;
        private long timeStamp;

        public ParcelObserved(String parcelId_, String stationId_, long timeStamp_) {
            this.parcelId = parcelId_;
            this.stationId = stationId_;
            this.timeStamp = timeStamp_;
        }

        public String toString() {
            return "ParcelID: " + parcelId + ", StationID: " + stationId + ", TimeStamp: " + timeStamp;
        }

        public String getParcelId() { return parcelId; }
        public String getStationId() { return stationId; }
        public long getTimeStamp() { return timeStamp; }
    }

    private static final ConcurrentMap<String, ConcurrentSkipListSet<ParcelObserved>> parcelMap = new ConcurrentHashMap<>(); //change to ConcurrentSkipList
    private static final ConcurrentMap<String, Long> stationCount = new ConcurrentHashMap<>();

    void postObserve(String parcelId, String stationId, long timeStamp) {
        ParcelObserved parcelObserved = new ParcelObserved(parcelId, stationId, timeStamp);
        ConcurrentSkipListSet<ParcelObserved> parcelList = parcelMap.getOrDefault(parcelId, new ConcurrentSkipListSet<>(new Comparator<ParcelObserved>() {
            public int compare(ParcelObserved a, ParcelObserved b) {
                return Long.compare(a.getTimeStamp(), b.getTimeStamp());
            }
        })); // new ConcurrentSkipListSet<>(new Comparator<ParcelObserved> )
        Long parcelCount = stationCount.getOrDefault(stationId, 0L);

        parcelList.add(parcelObserved);
        DataModel.parcelMap.put(parcelId, parcelList);
        DataModel.stationCount.put(stationId, parcelCount + 1L);
    }

    List<ParcelObserved> getParcelTrail(String parcelId) {
        return new ArrayList<>(parcelMap.getOrDefault(parcelId, new ConcurrentSkipListSet<>()));
    }

    long getStopCount(String stationId) {
        return DataModel.stationCount.getOrDefault(stationId, 0L);
    }

    long getTrailCount(String parcelId) {
        return parcelMap.getOrDefault(parcelId, new ConcurrentSkipListSet<>()).size();
    }
}
